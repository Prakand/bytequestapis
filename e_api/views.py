from django.shortcuts import render

from rest_framework import status, request
from rest_framework.decorators import api_view
from rest_framework.response import Response
from e_api.serializers import ProductSerializer
from e_api.models import Product


@api_view(['GET', 'POST'])
def product_details(request):
    # it will fetch all the data in database if method will be 'GET'
    if request.method == 'GET':
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

# it will post the data in database

    if request.method == 'POST':
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


@api_view(['GET', 'PUT', 'DELETE'])
def product_update(request, pk):

    # fetch single data from the database using primary key

    if request.method == 'GET':
        try:
            product = Product.objects.get(pk=pk)
        except Exception as e:
            return Response({"Error": "Movie not found"}, status=status.HTTP_404_NOT_FOUND)
        serializer = ProductSerializer(product)
        return Response(serializer.data)

# update the single data using primary key

    if request.method == 'PUT':
        try:
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response({"Error": "Product not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = ProductSerializer(product, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# delete data from database using primary key

    if request.method == 'DELETE':
        product = Product.objects.get(pk=pk)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





