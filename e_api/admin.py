from django.contrib import admin
from e_api.models import Product

# Register your models here.
admin.site.register(Product)
