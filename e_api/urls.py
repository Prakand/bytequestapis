from django.urls import path, include

from e_api import views

urlpatterns = [
    path("", views.product_details, name='get-data'),
    path('<int:pk>/', views.product_update, name='update-data'),
]
